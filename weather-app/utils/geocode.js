const request = require('postman-request')

geocode =(address,callback)=> {
    //https://api.mapbox.com/geocoding/v5/mapbox.places/chiangrai.json?access_token=pk.eyJ1Ijoienhjdm1hZ2ljIiwiYSI6ImNrZnFmeDJjYTAzeGgycW84bWZwcmxseHYifQ.oNhBt9TO8dIyQCXvmYuF6Q&limit=1
    const GeoUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+address+".json?access_token=pk.eyJ1Ijoienhjdm1hZ2ljIiwiYSI6ImNrZnFmeDJjYTAzeGgycW84bWZwcmxseHYifQ.oNhBt9TO8dIyQCXvmYuF6Q&limit=1"
    request({url:GeoUrl , json:true},(error,response)=>{
        if(error){
            callback(error, undefined)
        } else if(response.body.features.length === 0){
            callback('unable to find location',undefined)
        } else {
            callback(undefined,{
                latitude :response.body.features[0].center[1],
                longtitude : response.body.features[0].center[0],
                location: response.body.features[0].place_name
            })
        }
    })
}

module.exports = geocode