const request = require('postman-request')

// geocode =(address,callback)=> {
//     //? => %3F
//     const GeoUrl = "https://api.mapbox.com/geocoding/v5/mapbox.places/"+encodeURIComponent(address)+".json?access_token=pk.eyJ1Ijoienhjdm1hZ2ljIiwiYSI6ImNrZnFmeDJjYTAzeGgycW84bWZwcmxseHYifQ.oNhBt9TO8dIyQCXvmYuF6Q"
//     request({url:GeoUrl , json:true},(error,response)=>{
//         if(error){
//             callback('unable to connect to newwork', undefined)
//         } else if(response.body.features.length === 0){
//             callback('unable to find location',undefined)
//         } else {
//             callback(undefined,{
//                 latitude : response.body.features[0].center[1],
//                 longtitude : response.body.features[0].center[0],
//                 location: response.body.features[0].place_name
//             })
//         }
//     })
// }

forecast =(latitude,longitude,callback) =>{
   // http://api.weatherstack.com/current?access_key=e6f55ea0494d3d4ea50779d01f68a522&query=99.8746405, 19.957209
//'http://api.weatherstack.com/current?access_key=e6f55ea0494d3d4ea50779d01f68a522&query=19.957209,99.8746405&units=f
    const laUrl ='http://api.weatherstack.com/current?access_key=e6f55ea0494d3d4ea50779d01f68a522&query='+latitude+','+longitude+"&units=f"
    request({url:laUrl,json:true},(error,response) =>{
        if(error){
            callback(error, undefined)
        } else if(response.body.error){
            callback('unable to find location',undefined)
        } else {
            callback(undefined,console.log(response.body.current.weather_descriptions))
        }
    })
}

module.exports = forecast