const chalk = require('chalk')
const { argv } = require('yargs')
const yargs = require('yargs')
const Notes = require('./note')
//const GetNote = require('./utils')

//Text with Colors
//console.log(chalk.bgGreen.bold('Success'))

//Attribite of Executing
//const command = process.argv[2]
//if (command === 'add'){ console.log('Added')}

//Yargs = Tool of Manage Attribite of Executing
//Print yargs
//console.log(yargs.argv)

//Create Add Command
yargs.command({
    command:'add',
    discription:'Add new note',
    builder:{
        title : {
            description:'Note Tiltle',
            demandOption : true,
            type : 'string'
                },
        body:{
            discription : 'Note body',
            demandOption : true ,
            type : 'string'
        }
       
    },
    handler(argv){
        Notes.addNote(argv.title ,argv.body)
    }
})

//Create Remove Command
yargs.command({
    command:'remove',
    discription:'remove a note',
    builder:{
        title : {
            description:'Note Tiltle',
            demandOption : true,
            type : 'string'
                }
            },
    handler(){
        Notes.removeNote(argv.title)
    }
})

//Create List Command
yargs.command({
    command:'list',
    discription:'list a note',
    handler(){
        Notes.listNote()
    }
})

//Create Read Command
yargs.command({
    command:'read',
    discription:'Read a note',
    builder:{
        title : {
            description:'Note Tiltle',
            demandOption : true,
            type : 'string'
            }},            
    handler(){
        Notes.readNote (argv.title)

    }
})

yargs.parse()

