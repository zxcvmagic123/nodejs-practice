const fs = require('fs')
const chalk = require('chalk')
const { title } = require('process')

const loadNote = () => {

    try{
        const dataBuffer = fs.readFileSync('notes.json')
        return JSON.parse(dataBuffer.toString())
    } catch(e){
        return []
    }

    
}

const saveNote = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json',dataJSON)
}

const addNote =  (title, author) => {
    const notes = loadNote()
    const duplicateNotes = notes.filter((notes) => notes.title === title)
    // const duplicateNotes = note.find((notes)=> notes.title === title) <-- return defied/undefinded

    if(duplicateNotes.length === 0){
        notes.push({
            title : title,
            author : author
        })
        saveNote(notes)
        console.log('New Note Added')
    } else {
        console.log('Title has taken!')
    }
}

const removeNote = (title) => {
    const notes = loadNote()
    const noteToKeep = notes.filter((note) => note.title !== title)
    // const noteToKeep = notes.filter(function(notes){
    //     return notes.title !== title
    // })

    if(notes.length > noteToKeep.length){
        saveNote(noteToKeep)
        console.log(chalk.green.inverse("Note Removed"))
    }else{
        console.log(chalk.red.inverse("No Note Found"))
    }
    
}

const listNote =() =>{
    const notes = loadNote()
    console.log(chalk.blue.inverse("Your Notes"))
    notes.forEach((note) => {
        console.log("Title: "+ note.title)
    });
    
}

const readNote =(title) => {
    const notes = loadNote()
    const note = notes.find((notes)=> notes.title === title)

    if(note !== undefined){
        console.log(note.title + note.author)
    }else{
        console.log(chalk.red.inverse("No Note Found!"))
    }
}

module.exports ={
    addNote : addNote,
    removeNote: removeNote,
    listNote: listNote,
    readNote : readNote
}
